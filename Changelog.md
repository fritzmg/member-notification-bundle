# Changes in srhinow/memeber-notification-bundle
## 0.1.1 (01.09.2022)
- Fix Member::doMemberNotification() for use in Frontend Environment

## 0.1.0 (20.08.2020)
- add first running Version