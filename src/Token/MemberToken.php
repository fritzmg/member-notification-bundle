<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension member-notification-bundle.
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license LGPL-3.0-or-later
 */

namespace Srhinow\MemberNotificationBundle\Token;

use Contao\BackendUser;
use Contao\Database;
use Contao\Environment;
use Contao\FilesModel;
use Contao\Idna;
use Contao\MemberModel;

class MemberToken
{
    private $token = [];

    public function __construct()
    {
    }

    /**
     * Close the database connection.
     */
    public function __destruct()
    {
        $this->token = [];
    }

    /**
     * Prevent cloning of the object (Singleton).
     */
    final public function __clone()
    {
    }

    /**
     * Return an array item.
     *
     * @param string $strKey The property name
     *
     * @return string|null The property value
     */
    public function __get($strKey)
    {
        return $this->token[$strKey];
    }

    /**
     * @param $strKey
     * @param $strVal
     */
    public function __set($strKey, $strVal): void
    {
        $this->token[$strKey] = $strVal;
    }

    public function getTokenArray()
    {
        return $this->token;
    }

    /**
     * setzt die Felder von tl_member und tl_spree_case als default.
     */
    public function setDefaultToken(): void
    {
        $memberToken = $this->getMemberFieldsForTypeArray();
        $userToken = $this->getUserFieldsForTypeArray();

        $this->token = array_merge(
            $memberToken,
            $userToken
        );
    }

    public function getDefaultTypeArray()
    {
        $arrType['spree_casemanager']['spree_tokens'] = [];

        $memberToken = $this->getMemberFieldsForTypeArray();
        $userToken = $this->getUserFieldsForTypeArray();

        $arrType['spree_casemanager']['spree_tokens']['recipients'] = ['member_email'];

        $arrType['spree_casemanager']['spree_tokens']['email_sender_address'] = array_merge(
            $memberToken,
            $userToken
        );
        $arrType['spree_casemanager']['spree_tokens']['email_sender_name'] = array_merge(
            $memberToken,
            $userToken
        );
        $arrType['spree_casemanager']['spree_tokens']['email_subject'] = array_merge(
            $memberToken,
            $userToken
        );

        $arrType['spree_casemanager']['spree_tokens']['email_text'] = array_merge(
            $memberToken,
            $userToken
        );

        $arrType['spree_casemanager']['spree_tokens']['email_html'] = array_merge(
            $memberToken,
            $userToken
        );

        $arrType['spree_casemanager']['spree_tokens']['attachment_tokens'] = [];

        return $arrType;
    }

    /**
     * setzt alle Felder als verfügbare Tokens der tl_member.
     *
     * @return array
     */
    public function getMemberFieldsForTypeArray()
    {
        $arrReturn = [];
        $arrWhitelist = ['anrede', 'firstname', 'lastname', 'dateOfBirth', 'gender', 'company', 'street', 'postal',
            'city', 'state', 'country', 'phone', 'mobile', 'fax', 'email', 'website', 'language', 'username',
            'dateAdded', 'lastLogin', 'addressText', 'datenschutz', ];
        $objMemberTable = Database::getInstance()->listFields('tl_member');

        if (!\is_array($objMemberTable) || \count($objMemberTable) < 1) {
            return $arrReturn;
        }

        foreach ($objMemberTable as $field) {
            //nur Felder in Token aufnehmen die zuvor definiert wurden
            if (!\in_array($field['name'], $arrWhitelist, true)) {
                continue;
            }
            $arrReturn[] = 'member_'.$field['name'];
        }

        return $arrReturn;
    }

    /**
     * setzt alle Felder als verfügbare Tokens der tl_user.
     *
     * @return array
     */
    public function getUserFieldsForTypeArray()
    {
        $arrReturn = [];
        $arrWhitelist = ['username ', 'name', 'language', 'email', 'dateAdded'];

        $objUserTable = Database::getInstance()->listFields('tl_user');

        if (!\is_array($objUserTable) || \count($objUserTable) < 1) {
            return $arrReturn;
        }

        foreach ($objUserTable as $field) {
            //nur Felder in Token aufnehmen die zuvor definiert wurden
            if (!\in_array($field['name'], $arrWhitelist, true)) {
                continue;
            }

            $arrReturn[] = 'user_'.$field['name'];
        }

        return $arrReturn;
    }

    public function setNcDefaultToken(): void
    {
        $this->token['domain'] = Idna::decode(Environment::get('host'));
        $this->token['admin_email'] = $GLOBALS['TL_ADMIN_EMAIL'];
        $this->token['admin_name'] = $GLOBALS['TL_ADMIN_NAME'];
    }

    /**
     * set user Token as Agent-Token from actual BackendUser.
     *
     * @return bool | void
     */
    public function setUserToken()
    {
        $BeUser = BackendUser::getInstance();

        if (!\is_object($BeUser)) {
            return false;
        }

        $objUserTable = Database::getInstance()->listFields('tl_user');

        if (!\is_array($objUserTable) || \count($objUserTable) < 1) {
            return false;
        }

        foreach ($objUserTable as $field) {
            //keinen Token mit null übergeben
            if (null === $BeUser->{$field['name']}) {
                continue;
            }

            //keinen Token mit leerem String
            if ('' === $BeUser->{$field['name']}) {
                continue;
            }

            //Wert formatieren
            $value = $BeUser->{$field['name']};

            //Binary-Felder in Pfade umwandeln
            if ('fileTree' === $GLOBALS['TL_DCA']['tl_user']['fields'][$field['name']]['inputType']) {
                if ('radio' === $GLOBALS['TL_DCA']['tl_user']['fields'][$field['name']]['eval']['fieldType']) {
                    $objFile = FilesModel::findByUuid($value);
                    if (null === $objFile) {
                        continue;
                    }

                    $value = $objFile->path;
                }

                if ('checkbox' === $GLOBALS['TL_DCA']['tl_user']['fields'][$field['name']]['eval']['fieldType']) {
                    $objFiles = FilesModel::findMultipleByUuids($value);
                    if (null === $objFiles) {
                        continue;
                    }

                    while ($objFiles->next()) {
                        $value[] = $objFiles->path;
                    }
                }
            }

            $this->token['user_'.$field['name']] = $value;
        }
    }

    /**
     * @return bool | void
     */
    public function setMemberToken(MemberModel $member)
    {
        if (!\is_object($member)) {
            return false;
        }

        $objMemberTable = Database::getInstance()->listFields('tl_member');

        if (!\is_array($objMemberTable) || \count($objMemberTable) < 1) {
            return false;
        }

        foreach ($objMemberTable as $field) {
            //keinen Token mit null übergeben
            if (null === $member->{$field['name']}) {
                continue;
            }

            //keinen Token mit leerem String
            if ('' === $member->{$field['name']}) {
                continue;
            }

            //Wert formatieren
            $value = $member->{$field['name']};

            //Binary-Felder in Pfade umwandeln
            if ('fileTree' === $GLOBALS['TL_DCA']['tl_member']['fields'][$field['name']]['inputType']) {
                if ('radio' === $GLOBALS['TL_DCA']['tl_member']['fields'][$field['name']]['eval']['fieldType']) {
                    $objFile = FilesModel::findByUuid($value);
                    if (null === $objFile) {
                        continue;
                    }

                    $value = $objFile->path;
                }

                if ('checkbox' === $GLOBALS['TL_DCA']['tl_member']['fields'][$field['name']]['eval']['fieldType']) {
                    $objFiles = FilesModel::findMultipleByUuids($value);
                    if (null === $objFiles) {
                        continue;
                    }

                    while ($objFiles->next()) {
                        $value[] = $objFiles->path;
                    }
                }
            }

            $this->token['member_'.$field['name']] = $value;
        }
    }

    public function getToken(): array
    {
        return $this->token;
    }

    public function setToken(array $token): void
    {
        $this->token = $token;
    }
}
