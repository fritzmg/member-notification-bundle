<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension member-notification-bundle.
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license LGPL-3.0-or-later
 */

namespace Srhinow\MemberNotificationBundle\EventListener\Dca;

use Contao\Backend;
use Contao\BackendUser;
use Contao\DataContainer;
use Contao\FrontendUser;
use Contao\MemberModel;
use Contao\Message;
use NotificationCenter\Model\Notification;
use Srhinow\MemberNotificationBundle\Token\MemberToken;

class Member extends Backend
{
    /**
     * Import the back end user object.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * gibt die select-options zurück mit allen Notifications.
     *
     * @return array
     */
    public function getNotificationOptions(DataContainer $dc)
    {
        $options = [];

        $objNotifications = Notification::findAll();
        if (null === $objNotifications) {
            return $options;
        }

        while ($objNotifications->next()) {
            $options[$objNotifications->id] = $objNotifications->title;
        }

        return $options;
    }

    /**
     * führt beim Speichern, wenn ausgewählt eine Notification aus.
     *
     * @param DataContainer| FrontendUser $dc
     * @return bool | void
     */
    public function doMemberNotification($dc)
    {
        // Front end call
        if (!$dc instanceof DataContainer)
        {
            return;
        }

        // Return if there is no active record (override all)
        if (!$dc->activeRecord)
        {
            return;
        }

        if ((int) $dc->activeRecord->notificationId < 1) {
            return;
        }

        //Member-Datensatz holen
        $objMember = MemberModel::findByPk($dc->id);
        if (null === $objMember) {
            return;
        }

        $objNotification = Notification::findByPk($dc->activeRecord->notificationId);

        if (null !== $objNotification) {
            $TokenClass = new MemberToken();
            $TokenClass->setNcDefaultToken();
            $TokenClass->setMemberToken($objMember);
            if (BackendUser::getInstance()->id) {
                $TokenClass->setUserToken();
            }
            $arrTokens = $TokenClass->getToken();

            $arrMessage = $objNotification->send($arrTokens); // Language is optional

            if (!\is_array($arrMessage) || \count($arrMessage) < 1) {
                Message::addError($GLOBALS['TL_LANG']['tl_member']['notification_send_error']);
            } else {
                Message::addConfirmation($GLOBALS['TL_LANG']['tl_member']['notification_send_success']);
            }
        }

        //Notification-Felder zurueck setzen
        $objMember->notificationId = 0;
        $objMember->save();
    }
}
