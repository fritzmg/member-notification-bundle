<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension member-notification-bundle.
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license LGPL-3.0-or-later
 */

namespace Srhinow\MemberNotificationBundle;

use Srhinow\MemberNotificationBundle\DependencyInjection\SrhinowMemberNotificationBundleExtension;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Configures the Contao simple-map-bundle.
 */
class SrhinowMemberNotificationBundle extends Bundle
{
    /**
     * Builds the bundle.
     *
     * It is only ever called once when the cache is empty.
     *
     * This method can be overridden to register compilation passes,
     * other extensions, ...
     *
     * @param ContainerBuilder $container A ContainerBuilder instance
     */
    public function build(ContainerBuilder $container): void
    {
    }

    /**
     * {@inheritdoc}
     */
    public function getContainerExtension()
    {
        return new SrhinowMemberNotificationBundleExtension();
    }
}
