<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension member-notification-bundle.
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license LGPL-3.0-or-later
 */

$GLOBALS['TL_DCA']['tl_member']['config']['onsubmit_callback'][] = [
    'srhinow.member_notification_bundle.listeners.dca.member',
    'doMemberNotification',
];

$GLOBALS['TL_DCA']['tl_member']['palettes']['default'] .= ';{notification_legend},notificationId';

/*
 * Fields
 */
$GLOBALS['TL_DCA']['tl_member']['fields']['notificationId'] =
    [
        'label' => &$GLOBALS['TL_LANG']['tl_member']['notificationId'],
        'exclude' => true,
        'search' => true,
        'inputType' => 'select',
        'foreignKey' => 'tl_nc_notification.title',
        'options_callback' => ['srhinow.member_notification_bundle.listeners.dca.member', 'getNotificationOptions'],
        'eval' => ['doNotCopy' => true, 'includeBlankOption' => true, 'chosen' => true, 'tl_class' => 'w50'],
        'sql' => "int(10) unsigned NOT NULL default '0'",
        'relation' => ['type' => 'hasOne', 'load' => 'lazy'],
    ];
